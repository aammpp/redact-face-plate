CREATE TABLE IF NOT EXISTS apestel.redaction_table (
    date_c TIMESTAMP,
    uri_c STRING,
    type_c STRING,
    json_c STRING);
CREATE VIEW IF NOT EXISTS apestel.redaction_table_v as
SELECT
    date_c as date,
    uri_c as uri,
    type_c as type,
    COALESCE(
        array_length(json_extract_array(json_c, '$.faceAnnotations')),
        array_length(json_extract_array(json_c, '$.annotation_results[0].face_detection_annotations'))
    ) as faces,
    COALESCE(
        array_length(json_extract_array(json_c, '$.textAnnotations')),
        array_length(json_extract_array(json_c, '$.annotation_results[0].text_annotations'))
    ) as texts,
    array_length(json_extract_array(json_c, '$.faceAnnotations')) as image_faces, 
    array_length(json_extract_array(json_c, '$.textAnnotations')) as image_texts, 
    array_length(json_extract_array(json_c, '$.annotation_results[0].face_detection_annotations')) as video_faces, 
    array_length(json_extract_array(json_c, '$.annotation_results[0].text_annotations')) as video_texts, 
    json_c 
FROM `apestel.apestel.redaction_table`;
SELECT * EXCEPT (json_c) FROM apestel.redaction_table_v;

