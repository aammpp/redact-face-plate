from moviepy.editor import VideoFileClip
from google.cloud import videointelligence_v1, storage, bigquery
import os
from google.protobuf.json_format import MessageToJson
import json
import datetime

video_client = videointelligence_v1.VideoIntelligenceServiceClient()
storageClient = storage.Client()
bqClient = bigquery.Client()

def redact_video(event, context):

  # Get the new file
  bucket_name = event['bucket']
  file_name = event['name']
  uri = f"gs://{bucket_name}/{file_name}"

  print("Enter redact_video() for: " + uri)

  # Skip files in the redacted folder
  if file_name.startswith('redacted'):
    return

  config = videointelligence_v1.FaceDetectionConfig(
    include_bounding_boxes=True, include_attributes=False
  )
  video_context = videointelligence_v1.VideoContext(face_detection_config=config)

  features = [
    videointelligence_v1.Feature.FACE_DETECTION,
    videointelligence_v1.Feature.TEXT_DETECTION
  ]

  print("Getting Video AI annotations for: " + uri)
  operation = video_client.annotate_video(
    request={
      "features": features,
      "input_uri": uri,
      "video_context": video_context
    }
  )
  print("\nProcessing video for annotations for: " + uri)
  result = operation.result(timeout=180)

  # Store response in BQ
  print("\nStoring annotation results in BQ for: " + uri)
  errors = bqClient.insert_rows_json(
    "apestel.redaction_table", 
    [{  "date_c": str(datetime.datetime.now()),
        "type_c": "video",
        "uri_c": f"gs://{bucket_name}/redacted/{file_name}",
        "json_c": json.dumps(videointelligence_v1.types.AnnotateVideoResponse.to_dict(result))}])
  if errors == []:
    print("New rows have been added.")
  else:
    print("Encountered errors while inserting rows: {}".format(errors))


  # Download the image locally
  bucket = storageClient.bucket(bucket_name)
  blob = bucket.get_blob(file_name)
  src_temp_file = '/tmp/'+file_name
  dst_temp_file = '/tmp/dest'+file_name
  blob.download_to_filename(src_temp_file)
    
  clip = VideoFileClip(src_temp_file)


  def mask(gf, t):

    frame = gf(t).copy()

    for annotation in result.annotation_results[0].face_detection_annotations:

      for track in annotation.tracks:
        start = track.segment.start_time_offset.seconds + \
          track.segment.start_time_offset.microseconds / 1e6
        end = track.segment.end_time_offset.seconds + \
          track.segment.end_time_offset.microseconds / 1e6

        if(t >= (start-0.2) and t <= (end+0.2)):
          for timestamped_object in track.timestamped_objects:
            time = timestamped_object.time_offset.seconds \
              + timestamped_object.time_offset.microseconds / 1e6

            if(abs(time-t)<=0.2):
              box = timestamped_object.normalized_bounding_box
              frame[
                round(box.top*clip.h) : round(box.bottom*clip.h), 
                round(box.left*clip.w) : round(box.right*clip.w)
              ] = [0,0,0]

    for text_annotation in result.annotation_results[0].text_annotations:

      for text_segment in text_annotation.segments:
        start = text_segment.segment.start_time_offset.seconds \
          + text_segment.segment.start_time_offset.microseconds / 1e6
        end = text_segment.segment.end_time_offset.seconds \
          + text_segment.segment.end_time_offset.microseconds /1e6

        if(t >= (start-0.2) and t <= (end+0.2)):

          for f in text_segment.frames:

            time = f.time_offset.seconds \
              + f.time_offset.microseconds / 1e6
            if(abs(time-t)<=0.2):

              min_x = 99999999 
              min_y = 99999999
              max_x = 0 
              max_y = 0
              for vertex in f.rotated_bounding_box.vertices:
                if(vertex.x < min_x): min_x = vertex.x 
                if(vertex.y < min_y): min_y = vertex.y 
                if(vertex.x > max_x): max_x = vertex.x 
                if(vertex.y > max_y): max_y = vertex.y
                frame[
                  round(min_y*clip.h) : round(max_y*clip.h), 
                  round(min_x*clip.w) : round(max_x*clip.w)
                ] = [0,0,0]

    return frame

  print("Redacting faces and text for: " + uri)
  newclip = clip.fl(mask, apply_to='mask')
  newclip.write_videofile(dst_temp_file, audio=False, logger=None)

  # Upload the redacted video to GCS
  print("Uploading redacted version of: " + uri)
  new_blob = bucket.blob('redacted/'+file_name)
  new_blob.upload_from_filename(dst_temp_file)

  # Delete the video to free up the RAM file system
  os.remove(src_temp_file)
  os.remove(dst_temp_file)
