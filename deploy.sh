BUCKET_NAME=apestel-redact-video

#bq mk -t my_dataset.my_table json:string

gcloud functions deploy ${FUNCTION_NAME} \
    --entry-point redact \
    --runtime python39 \
    --trigger-resource ${BUCKET_NAME} \
    --trigger-event google.storage.object.finalize \
    --memory 8192MB \
    --timeout 540 \
    --source .
