from google.cloud import vision, storage, bigquery
from PIL import Image, ImageDraw
import os
import datetime

imageAnnotatorClient = vision.ImageAnnotatorClient()
storageClient = storage.Client()
bqClient = bigquery.Client()

def redact_image(event, context):
  # Get the new file
  bucket_name = event['bucket']
  file_name = event['name']
  uri = f"gs://{bucket_name}/{file_name}"

  print("Enter redact_image() for: " + uri)

  # Skip files in the redacted folder
  if file_name.startswith('redacted'):
    return

  # Use Vision AI to get the location of faces and text
  print("Getting Vision AI annotations for: " + uri)
  response = imageAnnotatorClient.annotate_image({
    'image': {'source': {'image_uri': uri}},
    'features': [
      {'type_': vision.Feature.Type.FACE_DETECTION},
      {'type_': vision.Feature.Type.DOCUMENT_TEXT_DETECTION}
    ]
  })

  # Store response in BQ
  print("Storing BQ row for: " + uri)
  errors = bqClient.insert_rows_json(
    "apestel.redaction_table", 
    [{  "date_c": str(datetime.datetime.now()),
        "type_c": "image",
        "uri_c": f"gs://{bucket_name}/redacted/{file_name}",
        "json_c": vision.AnnotateImageResponse.to_json(response)}])
  if errors == []:
    print("New rows have been added.")
  else:
    print("Encountered errors while inserting rows: {}".format(errors))

  # Download the image locally
  bucket = storageClient.bucket(bucket_name)
  blob = bucket.get_blob(file_name)
  temp_file = '/tmp/'+file_name
  blob.download_to_filename(temp_file)

  # Open the image to black out images and text
  source_img = Image.open(temp_file)
  draw = ImageDraw.Draw(source_img)

  # Black out faces
  print("Redacting faces for: " + uri)
  for face in response.face_annotations:
    v = face.bounding_poly.vertices
    box = [v[0].x, v[0].y, v[2].x, v[2].y]
    draw.rectangle(box, fill='black')

  # Black out text
  print("Redacting text for: " + uri)
  for page in response.full_text_annotation.pages:
    for block in page.blocks:
      v = block.bounding_box.vertices
      box = [v[0].x, v[0].y, v[2].x, v[2].y]
      draw.rectangle(box, fill='black')

  # Upload the redacted image to GCS
  print("Uploading redacted version of: " + uri)
  source_img.save(temp_file)
  new_blob = bucket.blob('redacted/'+file_name)
  new_blob.upload_from_filename(temp_file)

  # Delete the image to free up the RAM file system
  os.remove(temp_file)