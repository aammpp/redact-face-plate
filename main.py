import mimetypes
from redact_image import redact_image
from redact_video import redact_video
mimetypes.init()

def redact(event, context):
  file_name = event['name']
  mimestart = mimetypes.guess_type(file_name)[0]

  if mimestart == None:
    print('Unknown file type: ' + file_name)

  elif mimestart.split('/')[0] == 'image':
    redact_image(event, context)

  elif mimestart.split('/')[0] == 'video':
    redact_video(event, context)

  else:
    print('Unknown file type: ' + file_name)
